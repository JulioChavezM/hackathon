import { LitElement, html, css } from 'lit-element';
import './inversiones-listado'
export class InversionesPropuesta extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    static get properties() {
        return {
            inversiones: {type: Array},
            dataCliente: {type: Array},
        };
    }

    constructor() {
        super();
        this.inversiones=[];
        this.dataCliente=[];
        this.getData();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            
            <div class="container table-responsive table-hover">
                <div class="row align-items-start">
                    <div class="col d-inline p-4 bg-primary text-white rounded-left" scope="col">Intrumento</div>
                    <div class="col d-inline p-4 bg-primary text-white " scope="col">MontoMax</div>
                    <div class="col d-inline p-4 bg-primary text-white " scope="col">MontoMin</div>
                    <div class="col d-inline p-4 bg-primary text-white " scope="col">MontoInv</div>
                    <div class="col d-inline p-4 bg-primary text-white " scope="col">Plazo</div>
                    <div class="col d-inline p-4 bg-primary text-white " scope="col">Tasa</div>
                    <div class="col d-inline p-4 bg-primary text-white rounded-right" scope="col">Riesgo</div>
                </div>                    
                    ${this.inversiones.map(
                        inversion =>
                        html`
                            <inversiones-listado
                                .dataPropuesta="${inversion}"
                                .dataCliente="${this.dataCliente}"
                                @select-item="${this.selectItem}">
                            </inversiones-listado>      
                        `
                    )}
            </div>
        `;
    }

    selectItem(e){
        this.dispatchEvent(
            new CustomEvent("open-inversion", {
                    detail: {
                        dataPropuesta: e.detail.dataPropuesta
                    }
                }
            )
        );
    }

    getData(){
        let xhr = new XMLHttpRequest();
        xhr.onload = function (){
            if (xhr.status == 200){
                let apiResponse = JSON.parse(xhr.responseText);
                this.inversiones = apiResponse;
            }
        }.bind(this);
        xhr.open("GET", "http://localhost:9999/inversiones/v1/instrumentos");
        //xhr.open("GET", "http://localhost:8000/data/propuestas.json");
        xhr.send();
    }

}
customElements.define('inversiones-propuesta', InversionesPropuesta);