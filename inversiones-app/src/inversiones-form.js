import { LitElement, html, css } from 'lit-element';
import '@bbva-web-components/bbva-modal-alert/bbva-modal-alert';

export class InversionesForm extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    .form-inversiones{
        background-color: #E5E5E5;
        margin-top: 10px;
    }
    #msgError{
        color: red;
        font-weight: bold;
        font-size: 20px;
    }
    #formInversion{
        padding-top: 30px;
        padding-bottom: 30px;
    }
    .fixButtonExtra{
        padding-top: 70%;
    }
    .contrato{
        font-weight: bold;
    }

    `;

    static get properties(){
        return{
            inversion: {type: Object},
            nContrato: {type: String},
            dataCliente: {type: Array},
            monto: {type: Number}
        };
    }

    constructor(){
        super();
        this.inversion = {};
        this.dataCliente = [];
        this.nContrato = "";
        this.monto=0;
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <div class="container form-inversiones">
                <form id="formInversion">
                    <div class="form-group">
                        <label for="instrumento">Instrumento</label>
                        <input type="text" id="instrumento" @input="${this.updateInstrumento}" class="form-control" placeholder="Instrumento" .value="${this.fixUndefined(this.inversion.nombre)}" readonly />
                    </div>
                    <div class="form-group">
                        <label for="riesgo">Riesgo</label>
                        <input type="text" id="riesgo" @input="${this.updateRiesgo}" class="form-control" placeholder="Riesgo" .value="${this.fixUndefined(this.inversion.riesgo)}" readonly />
                    </div>
                    <div class="form-row">
                        <div class="col-md">
                            <div class="form-group">
                                <label for="plazo">Plazo</label>
                                <input type="text" id="plazo" @input="${this.updatePlazo}" class="form-control" placeholder="Plazo" .value="${this.fixUndefined(this.inversion.plazo)}" readonly />
                            </div>
                        </div>
                        <div class="col-md">
                            <div class="form-group">
                                <label for="tasa">Tasa</label>
                                <input type="text" id="tasa" @input="${this.updateTasa}" class="form-control" placeholder="Tasa" .value="${this.fixUndefined(this.inversion.tasa)}" readonly />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="monto">Monto a invertir</label>
                        <input type="number" id="monto" @input="${this.updateMonto}" class="form-control" placeholder="Monto a Invertir" .value="${this.fixUndefined(this.monto)}" />
                        <br>
                        <div id="msgError"></div>
                    </div>
                    <div class="form-row">
                        <div class="col-md">
                            <input type="button" @click="${this.aceptaInversion}" class="btn btn-primary btn-lg btn-block" value="Invertir">
                        </div>
                        <div class="col-md">
                            <input type="button" @click="${this.cancelaInversion}" class="btn btn-primary btn-lg btn-block" value="Cancelar">
                        </div>
                    </div>
                </form>
            </div>
            <div>
                <bbva-modal-alert header-text="Confirmaciòn de contrataciòn" />
                    <div class="content" slot="slot-content">
                        <div style="padding-top: 200px;">
                            <h3>Tu folio de confirmación de contrato.</h3>
                            <span class="contrato">
                                <h2>${this.nContrato}</h2>
                            </span>
                        </div>
                        <input type="button" @click="${this.closeModalForm}" class="btn btn-success btn-lg" value="Entendido.">
                        <div class="fixButtonExtra"></div>
                    </div>
                </bbva-modal-alert>
            </div>
            <div class="fixButtonExtra"></div>
        `;
    }

    updateInstrumento(e){
        this.inversion.instrumento = e.target.value;
    }

    updateRiesgo(e){
        this.inversion.riesgo = e.target.value;
    }

    updatePlazo(e){
        this.inversion.plazo = e.target.value;
    }

    updateTasa(e){
        this.inversion.taza = e.target.value;
    }

    updateMonto(e){
        if(!isNaN(e.target.value)){
            this.monto = e.target.value;
            this.shadowRoot.getElementById("msgError").innerHTML = "";
        }else{
            this.shadowRoot.getElementById("msgError").innerHTML = "Debe ingresar valores numericos unicamente.";
        }
    }

    aceptaInversion(){
        var bContinuaMenor = true;
        var bContinuaMayor = true;
        this.monto = parseInt(this.monto);
        if(this.monto > 0){
            if(this.monto < this.inversion.saldoMin){
                bContinuaMenor = false;
                this.shadowRoot.getElementById("msgError").innerHTML = `El monto a invertir no debe de ser mmenor a ${this.formatMoney(this.inversion.saldoMin)}`;
            }else{
                bContinuaMenor = true;
            }
            if(this.monto > this.inversion.saldoMax){
                bContinuaMayor = false;
                this.shadowRoot.getElementById("msgError").innerHTML = `El monto a invertir no debe de ser mayor a ${this.formatMoney(this.inversion.saldoMax)}`;
            }else{
                bContinuaMayor = true;
            }

            if(bContinuaMenor && bContinuaMayor){
                this.sendData();
                this.shadowRoot.getElementById("msgError").innerHTML = "";
                this.cleanForm();
            }
        }else{
            this.shadowRoot.getElementById("msgError").innerHTML = "El monto a invertir debe de ser mayor a 0";
        }
    }

    cancelaInversion(){
        this.dispatchEvent(new CustomEvent('inversion-cancela',{}));
        this.cleanForm();
    }

    cleanForm(){
        this.inversion = {};
    }

    fixUndefined(valor){
        return valor == undefined ? '' : valor;
    }

    sendData(){
        this.shadowRoot.querySelector('bbva-modal-alert').open();
        let url = 'http://localhost:9999/inversiones/v2/contratos';
        var params = JSON.stringify({
          "numeroCliente": this.dataCliente.numeroCliente,
          "idInstrumento": this.inversion.nombre,
          "monto": this.monto
        });
        let xhr = new XMLHttpRequest();
        xhr.onload = function(){
            if (xhr.status === 200) {
                let APIResponse = (xhr.responseText);
                this.nContrato = APIResponse;
            }
        }.bind(this);
        xhr.open("POST", url);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(params);

    }

    closeModalForm(){
        this.shadowRoot.querySelector('bbva-modal-alert').close();
        setTimeout(() => {
            this.dispatchEvent(new CustomEvent('inversion-acepta', {}));
        }, 300);
    }

    formatMoney(value){
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0
        });
        return formatter.format(value);
    }

}
customElements.define('inversiones-form', InversionesForm);
