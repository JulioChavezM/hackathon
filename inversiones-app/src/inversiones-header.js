import { LitElement, html, css } from 'lit-element';
import './inversiones-cliente'
export class InversionesHeader extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        
        <nav class="navbar navbar-inverse fixed-top bg-dark ">
            <div class="container-fluid">
                <div class="navbar-header text-white">
                    <a class="navbar-brand "><H2>Notificaci&oacute;n Ofertas para Fondos de Inversi&oacute;n</H2></a>
                </div>
                <inversiones-cliente></inversiones-cliente>
            </div>
        </nav> 
        `;
    }
}
customElements.define('inversiones-header', InversionesHeader);