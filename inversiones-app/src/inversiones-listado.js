import { LitElement, html, css } from 'lit-element';

export class InversionesListado extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    div.row:hover
    {
        background-color: #184CF2;
    }
    `;

    static get properties() {
        return {
            dataPropuesta: {type: Array},
            dataCliente: {type: Array}
        };
    }

    constructor() {
        super();
        this.dataPropuesta=[];
        this.dataCliente=[];
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            
            <div class="row" @click="${this.selectItem}">
                <div class="col d-inline p-2 border border-primary text-black rounded-left">${this.dataPropuesta.nombre}</div>
                <div  class="col d-inline p-2 border border-primary text-black">${this.formatMoney(this.dataPropuesta.saldoMax)}</div>
                <div  class="col d-inline p-2 border border-primary text-black">${this.formatMoney(this.dataPropuesta.saldoMin)}</div>
                <div  class="col d-inline p-2 border border-primary text-black">${this.formatMoney(this.dataCliente.saldoActual)}</div>
                <div  class="col d-inline p-2 border border-primary text-black">${this.dataPropuesta.plazo}</div>
                <div  class="col d-inline p-2 border border-primary text-black">${this.dataPropuesta.tasa}</div>
                <div  class="col d-inline p-2 border border-primary text-black rounded-right">${this.dataPropuesta.riesgo}</div>
            </div>            
        `;
    }

    selectItem(){
        this.dispatchEvent(
            new CustomEvent("select-item", {
                    detail: {
                        dataPropuesta: this.dataPropuesta
                    }
                }
            )
        );
    }

    formatMoney(value){
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'USD',
            minimumFractionDigits: 0
        });
        return formatter.format(value);
    }

}
customElements.define('inversiones-listado', InversionesListado);