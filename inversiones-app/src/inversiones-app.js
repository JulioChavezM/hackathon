import { LitElement, html, css } from 'lit-element';
import './inversiones-header';
import './inversiones-main';
import './inversiones-footer';

export class InversionesApp extends LitElement {
    static get properties() {
        return {
            title: { type: String },
        };
    }

    static get styles() {
        return css`
        :host {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: flex-start;
            font-size: calc(10px + 2vmin);
            color: #1a2b42;
            margin: 0 auto;
            text-align: center;
            background-color: var(--inversiones-app-background-color);
        } `;
    }

    constructor() {
        super();
        this.title = 'My app';
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <div class="col mt-5">
            <header>
                <inversiones-header></inversiones-header>
            </header>
        </div>
        <div class="col w-100 mt-5"> 
            <main>
                <inversiones-main @data-cliente=${this.dataCliente}></inversiones-main>
            </main>
        </div>
        <div class="col mt-5">
            <footer>
                <inversiones-footer></inversiones-footer>
            </footer>
        </div>
    `;
    }

    dataCliente(e){
        this.shadowRoot.querySelector("inversiones-header").shadowRoot.querySelector("inversiones-cliente").cliente=e.detail.dataCliente.nombreCliente+" "+e.detail.dataCliente.apellidoPaCliente;
    }

}
customElements.define('inversiones-app', InversionesApp);
