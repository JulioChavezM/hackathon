import { LitElement, html, css } from 'lit-element';

export class InversionesCliente extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    div{color:white;}
    `;

    static get properties(){
        return{
            cliente: {type: String}
        };
    }

    constructor(){
        super();
        this.cliente = "";
    }

    render() {
        return html`
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        
        <div class='float-right d-inline text-light mr-auto'>
            Bienvenido: ${this.cliente}
        </div>
        `;
    }
}
customElements.define('inversiones-cliente', InversionesCliente);