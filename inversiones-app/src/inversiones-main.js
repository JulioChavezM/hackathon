import { LitElement, html, css } from 'lit-element';
import './inversiones-form';
import '@bbva-web-components/bbva-modal-alert/bbva-modal-alert';
import './inversiones-propuesta';

export class InversionesMain extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    .footer-button{
        display: nonse;
    }
    main {
        margin-top:10px; margin-bottom:10px;
    }
    `;

    static get properties() {
        return {
            inversion: {type: Array},
            dataCliente: {type: Array}
        };
    }

    constructor(){
        super();
        this.inversion = {};
        this.dataCliente = {};
        this.getDataCliente();
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
            <main>
                <div class="w-100">
                    <inversiones-propuesta @open-inversion="${this.openInversion}"></inversiones-propuesta>
                    <bbva-modal-alert header-text="Tu Inversión" />
                        <div class="content" slot="slot-content">
                            <inversiones-form .inversion="${this.inversion}"
                            @inversion-cancela="${this.closeInversion}"
                            @inversion-acepta="${this.saveInversion}">
                            </inversiones-form>
                        </div>
                    </bbva-modal-alert>
                </div>
            </main>
        `;
    }

    openInversion(e) {
        this.inversion=e.detail.dataPropuesta;
        this.shadowRoot.querySelector('bbva-modal-alert').open();
    }

    closeInversion() {
        this.shadowRoot.querySelector('bbva-modal-alert').close();
    }

    saveInversion() {
        this.closeInversion();
    }

    getDataCliente(){
        let xhr = new XMLHttpRequest();
        xhr.onload = function (){
            if (xhr.status == 200){
                let apiResponse = JSON.parse(xhr.responseText);
                this.dataCliente = apiResponse;
                this.dispatchEvent(new CustomEvent("data-cliente",{"detail":{"dataCliente":this.dataCliente}}));
                this.shadowRoot.querySelector("inversiones-propuesta").dataCliente=apiResponse;
                this.shadowRoot.querySelector("inversiones-form").dataCliente=apiResponse;
                this.shadowRoot.querySelector("inversiones-form").monto=apiResponse.saldoActual;

            }
        }.bind(this);
        xhr.open("GET", "http://localhost:9999/inversiones/v1/clientes/60cbef705c5e570c86e3e9b4");
        //xhr.open("GET", "http://localhost:8000/data/usuario.json");
        xhr.send();
    }

}
customElements.define('inversiones-main', InversionesMain);
